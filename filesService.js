const fs = require('fs');
const path = require('path');

function createFile(req, res, next) {
    try {
        const {extension, content, filename} = req.body;
        fs.writeFileSync(`${process.env.PATHNAME}${filename}${extension}`, content);
        res.status(200).send({"message": "File created successfully"});
    } catch (e) {
        res.status(404).json({message: `Can't create file`})
    }
}

function getFiles(req, res, next) {
    try {
        const data = fs.readdirSync(`${process.env.PATHNAME}`);
        console.log(data);

        res.status(200).send({
            "message": "Success",
            "files": data
        });

    } catch (e) {
        res.status(404).json({message: `Can't read files from ${process.env.PATHNAME}`})
    }

}

const getFile = (req, res, next) => {
  try {
    const {filename} = req.params;
    const filePath = `${process.env.PATHNAME}${filename}`

    const content = fs.readFileSync(filePath, 'utf-8');
    const extension = path.extname(filename);

    const stats = fs.statSync(filePath);
    const fileCreationDate = stats.birthtime;

    res.status(200).send({
      "message": "Success",
      "filename": filename,
      "content": content,
      "extension": extension,
      "uploadedDate": fileCreationDate
    });
  }  catch (e) {
    res.status(404).json({message: `Can't read file`})
  }
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
    createFile,
    getFiles,
    getFile
}
